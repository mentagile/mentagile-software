cd /D "%~dp0"
cls
echo "Please have Python 3.7 or better installed"
py -3 -m venv env
.\env\Scripts\activate
deactivate
echo "Done."