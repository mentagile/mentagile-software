#!/bin/bash
clear
echo "Please have Python 3.7 or better installed"
cd -- "$(dirname "$BASH_SOURCE")" 
python3.7 -m venv env
source env/bin/activate
python3.7 -m pip install --upgrade pip
python3.7 -m pip install pyserial pygame
python3.7 mentagile_lab.py
deactivate
echo "Done."
