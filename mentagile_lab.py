# !python3

"""
Mentagile Lab (The Lab Controller)

All database requests must be passed through the data_recorder object,
preferrably using get or set methods in the data_recorder object but
access is also possible through the  data_recorder.sql_query(sql) and
data_recorder.sql_query("sql ? ?", (value1, value2)) methods

All serial communication must be passed through the serial_rekener object,
preferrably using get or set methods

By keeping these modules compartmentalized it will be easier in the
future to swap out a different database or the serial port for a bluetooth module
or for a different game.

Kenneth Dale user12@gmail.com 2020
"""
import sys
import os
from classes.color import Color
from configparser import ConfigParser
from classes.datarecorder import DataRecorder
from classes.serialreader import SerialReader
from classes.snakegame import SnakeGame

suggested_python = (3, 7, 5)
required_python = (3, 7, 0)
current_python = (sys.version_info[0], sys.version_info[1], sys.version_info[2])


def version_float(version_tuple): return version_tuple[0] + (version_tuple[1] / 10) + (version_tuple[2]/100)


def version_string(version_tuple): return str(version_tuple[0]) + "." + str(version_tuple[1]) + str(version_tuple[2])


if version_float(current_python) < version_float(required_python):
    print(Color.error + "This requires at least Python version " + version_string(required_python) + Color.normal)
    exit(1)


if version_float(current_python) != version_float(suggested_python):
    print(Color.warning + "Python version " + version_string(suggested_python) + " is recommended")
    print("Version: " + version_string(current_python) + " is currently running" + Color.normal)


def mentagile_lab():
    """
    mentagile_lab
    """
    print(Color.highlight + "\nWelcome to the mentagile lab\n" + Color.normal)
    # read configuration file
    # anything you might need to alter should be changed in the file settings.ini
    settings = ConfigParser()
    settings.read("settings.ini")

    data_recorder = DataRecorder(settings)
    serial_reader = SerialReader(settings, data_recorder)

    serial_reader.blocking_is_ready()
    data_recorder.confirm_current_user()
    loop_repeat = True
    while loop_repeat:

        data_recorder.start_session()

        game = SnakeGame(data_recorder, int(settings["snake_game"]["speed"]),
                         int(settings["snake_game"]["window_size"]),
                         int(settings["snake_game"]["rows"]), 0)

        print('Starting codecamp snake, please calm your mind.\n')
        game.play()

        print(Color.bold + "Please play again! (Seriously, we need the data.)")
        choice = input("([enter] for yes, [n] for no) > ")
        if len(choice) > 0 and choice[0:1] == "n":
            loop_repeat = False

        data_recorder.end_session()  # stops recording  data
        game.pygame_quit()

if __name__ == "__main__":
    # execute only if run as a script
    # this should always be the main script
    try:
        os.chdir(os.path.dirname(sys.argv[0]))
    except FileNotFoundError:
        pass
    mentagile_lab()
