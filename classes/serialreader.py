#!/usr/bin/env python3
"""
Serial Reader
@author: bryanjackson
@author: Kenneth Dale user12@gmail.com
"""

from time import sleep
import threading
import serial
from queue import Queue, Empty


class SerialReader:
    """
    Serial Reader connects to serial device (HR/BloodO2 sensor)
    """

    def __init__(self, settings, data_recorder):

        self._device = str(settings["serial_port"]["redboard_serial_port"])
        self._baudrate = int(settings["serial_port"]["baudrate"])
        self._parity = serial.PARITY_NONE
        self._stopbits = serial.STOPBITS_ONE
        self._bytesize = serial.EIGHTBITS
        self._timeout = int(settings["serial_port"]["timeout"])
        self._retry_connect = int(settings["serial_port"]["retry_connect"])
        self._data_queue = Queue()

        self.latest_data = {}
        self._data_recorder = data_recorder
        self._port_string = ''
        self._lines_received = 0
        self._lines_processed = 0
        self._serial_connection = None
        self.close_flag = False
        self.read_serial_thread = threading.Thread(target=self._read_serial)
        self.process_data_thread = threading.Thread(target=self._process_data)
        self.read_serial_thread.setDaemon(True)
        self.process_data_thread.setDaemon(True)
        self.connected = False
        self.set_device(self._device)
        self.red_board_time = None

    def blocking_is_ready(self):
        """does not return until the serial device is ready

        Returns:
            True  or does not return
        """
        while not self.connected:
            sleep(1)

        return True

    def is_ready(self):
        """returns true if the serial device is ready
        false if it is not ready
        """
        return self.connected

    def hr_extended_status(self, argument):
        """[converts the HR sensor code to a meaningful string]

        Arguments:
            argument {[int]} -- [the HR sensor exit code]

        Returns:
            [string] -- [The exit status string]
        """
        extended_status = {
            0:  'Success',
            1:  'Not Ready',
            -1: 'Object Detected',
            -2: 'Excessive Sensor Device Motion',
            -3: 'No object detected',
            -4: 'Pressing too hard',
            -5: 'Object other than finger detected',
            -6: 'Excessive finger motion'
        }
        return extended_status.get(argument, None)

    def hr_status(self, argument):
        """[converts the HR monitor code to a meaningful string]

        Arguments:
            argument {[integer]} -- [HR Sensor Code]

        Returns:
            [string] --  [status string]
        """
        status = {
            0: 'No Object Detected',
            1: 'Object Detected',
            2: 'Object Other Than Finger Detected',
            3: 'Finger Detected'
        }
        return status.get(argument, None)

    def set_device(self, device=False):
        """[sets the serial device]

        Keyword Arguments:
            device {string} -- [The serial port associated with the desired serial device
                                False returns false] (default: {False})
        """
        if device is False:
            return False
        self.close_flag = True
        self._port_string = '/dev/' + device
        return self._connect_device()

    def _connect_device(self):
        """[Connects to the device previously set with set_device(device)]
        """
        print("Connecting to the Red Board at "+self._port_string)
        success = False
        for attempt in range(0, self._retry_connect):
            try:
                self._serial_connection = serial.Serial(
                    port=self._port_string,
                    baudrate=self._baudrate,
                    parity=self._parity,
                    stopbits=self._stopbits,
                    bytesize=self._bytesize,
                    timeout=self._timeout)
                success = True
                self.connected = True
                # print("Connected to the Red Board") #+self._port_string)
                break
            except serial.SerialException as the_failure:
                print(the_failure)
                print("Serial Connect Failed try #" + str(attempt) + " Trying again in 10 seconds...")
                sleep(10)
        self.close_flag = False

        if success is False:
            print('Connect Failed')
            self.connected = False
        else:
            try:
                self.return_when_ready()
                self.process_data_thread.start()
            except RuntimeError:
                print("attempted to start already started process_data thread")
            try:
                self.read_serial_thread.start()
            except RuntimeError:
                print("attempted to start already started read_serial thread")
            return success

    def return_when_ready(self):
        waiting_for_ready = ""
        while (waiting_for_ready != "RUREADY"):
            waiting_for_ready = self._serial_connection.readline().decode("ascii", "ignore").strip()
        self._serial_connection.write(b'IAMREADY\n')
        self._serial_connection.flush()
        return True

    def _read_serial(self):
        """ This thread just reads from the serial port and stores each line in a queue
        It is necesarily simple as any processing delay could cause a buffer overflow
        and cause data to be lost or corrupted.  The data is processed in a separate
        thread _process_data
        """
        while not self.close_flag:
            try:
                newData = self._serial_connection.readline()
                if newData:
                    self._data_queue.put(newData)
                    self._lines_received += 1
                else:  # timeout happened
                    self.return_when_ready()
            except serial.SerialException:
                print("Error reading from serial device:")
                sleep(1)
                self._serial_connection.close()
                self.connected = False
                while not self.connected and not self.close_flag:
                    self._connect_device()
        self._serial_connection.close()

    def _process_data(self):
        """Processes the data in the received queue
        Runs as a separate thread
        """
        while not self.close_flag:
            try:
                line_bytes = self._data_queue.get(True, 2)  # blocks for 2 s before failing if no data is in the queue
                try:
                    line = line_bytes.decode("ascii", "ignore")
                    if line.strip() != "":  # ignore empty lines
                        self._lines_processed += 1
                        primary_id, line = line.split(", ", 1)
                        if primary_id == "SFRB":
                            sequence_id, secondary_id, value = line.split(", ", 2)
                            if secondary_id == "St":
                                if value == 0 or value == 2:  # nothing or note a finger detected
                                    self._data_recorder.set_finger(False)
                                else:  # a potential finger or a finger detected
                                    self._data_recorder.set_finger(True)
                            elif secondary_id == "TM":  # Store the TM (time) sync messages so that they can be processed by the RTC manager
                                self.red_board_time = line  # todo: this should be a queue or something.
                            self._data_recorder.add(primary_id, sequence_id, secondary_id, value)
                except (UnicodeDecodeError, ValueError) as error:
                    print("error: ", line_bytes)
                    print(error)
                    self._data_recorder.add_error(line_bytes)
            except Empty:
                sleep(.2)  # if the queue is empty, wait 300ms and check again

    def on_stop(self):
        """[summary]
        """
        self.close_flag = True
        sleep(1)
        self._data_recorder.add("SERIALREADER", self._data_recorder.millis() +
                                " , MSG, Serialreader shutdown (received, processed): " +
                                str(self._lines_received) + ", " + str(self._lines_processed))
