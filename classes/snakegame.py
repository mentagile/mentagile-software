# !/usr/bin/env python3
"""
Snake Tutorial from CodeCamp3

https://pastebin.com/embed_js/jB6k06hG

Modified by Bryan Jackson 2019
Modified by Kenneth Dale user12@gmail.com 2020
"""

from time import time
from time import sleep
from random import randrange
from classes.color import Color
import pygame

class SnakeGame:
    """
    SnakeGame
    """

    def __init__(self, data_recorder, game_delay = 100, game_width = 500, game_rows = 20, games_played = 0):
        """[summary]

        Arguments:
            data_recorder {[type]} -- [description]

        Keyword Arguments:
            game_delay {int} -- [description] (default: {100})
            game_width {int} -- [description] (default: {500})
            game_rows {int} -- [description] (default: {20})
            games_played {int} -- [description] (default: {0})

        Returns:
            [type] -- [description]
        """
        self.finger_detected = False
        self.data_recording_ok = False
        self.data_recorder = data_recorder
        self.game_width = game_width
        self.game_rows = game_rows
        self.games_played = games_played
        self.game_speed = self.set_game_speed()
        self.score = 1
        self.tf_score = 0
        self.game_time_diff = 0
        self.old_ttime = 0
        self.window = pygame.display.set_mode(
            (self.game_width, self.game_width))
        middle = int(self.game_rows/2)
        self.snake = Snake(self.game_width, self.game_rows, (255, 0, 0), (middle, middle))
        self.snack = self.get_random_snack()
        self.log_count = 0
        self.game_delay = game_delay

    def set_game_speed(self, games_played = None):
        """
        Gets the speed based on number of games played by user
        :param gamesplayed: number of games played by user
        :return: result out of gamespeedmap: gamespeed (smaller # is slower)
        """
        if games_played is None:
            games_played = self.games_played

        gamespeedmap = {
            0: 0.7,
            1: 0.72,
            2: 0.74,
            3: 0.75,
            4: 0.77,
            5: 0.80,
            6: 0.83,
            7: 0.82,
            8: 0.84,
            9: 0.87,
            10: 0.90,
            11: 0.95
        }
        # default speed set to really fast!
        self.game_speed = gamespeedmap.get(games_played, 1)
        return self.game_speed

    def draw_grid(self, surface):
        """
        draw_grid

        Raises:
            TypeError: [description]

        Returns:
            [type] -- [description]
        """
        cell_width = self.game_width // self.game_rows
        for line_position in range(0, self.game_width, cell_width):
            pygame.draw.line(surface, (255, 255, 255),
                                (line_position, 0), (line_position, self.game_width))
            pygame.draw.line(surface, (255, 255, 255),
                                (0, line_position), (self.game_width, line_position))

    def draw_hr(self, surface):
        """
        Draws a circle representing when a valid heart rate is being recorded

        Arguments:
            surface {[Surface]} -- [the pygame surface]
        """
        if self.data_recorder.finger_detected is False:
            pygame.draw.circle(surface, (255, 0, 0), (10, 10), 7, 2)
        else:
            pygame.draw.circle(surface, (0, 255, 0), (10, 10), 7, 4)
            if self.finger_detected == False:
                self.finger_detected = True
                print("Finger detected.")

    def redraw_window(self, surface):
        """
        redraws the pygame window
        """
        surface.fill((0, 0, 0))
        self.snake.draw(surface)
        self.snack.draw(surface)
        self.draw_grid(surface)
        self.draw_hr(surface)
        pygame.display.update()

    def log_event(self, snake_event, value):
        """[sends the event to the data_recorder]

        Arguments:
            snake_game_event {[string]} -- [this is appended to a idenifying counter specific to the snake game]
        """
        self.log_count += 1
        self.data_recorder.add("SNAKEGAME", self.data_recorder.millis(), snake_event, value)

    def get_random_snack(self):
        """
        random_snack
            Trys random positions for a new snack until one is
            found that is not where the snake is already
        """
        valid_choice = False
        while not valid_choice:
            potential_choice = (
                randrange(self.game_rows),
                randrange(self.game_rows)
            )
            if len(list(filter(
                lambda body_segment: body_segment.pos == potential_choice,
                self.snake.body))) == 0: valid_choice = True

        return Cube(self.game_width, self.game_rows, potential_choice, color = (0, 255, 0))

    def applesnakepositions(self):
        """[gets all the snake segment positions]

        Returns:
            [string] -- [all the positions as tuples, comma separated]
        """
        game_string = str(self.snack.pos)
        for box in self.snake.body:
            game_string += str(box.pos)
        return game_string


    def play(self, new_game_speed = None):
        """
        The main game loop
        """
        self.log_event("SNAKEGAMESTART", str(time()))
        self.log_event("SNAKEGAMEWIDTH", str(self.game_width))
        self.log_event("SNAKEGAMEROWS", str(self.game_rows))

        if new_game_speed is not None:
            self.game_speed = new_game_speed
        valid_keys = [pygame.K_LEFT, pygame.K_RIGHT,
                                pygame.K_UP, pygame.K_DOWN]
        game_delay = int(self.game_delay/self.game_speed)  # adjust gamespeed
        increase_snake_speed_by = None
        clock = pygame.time.Clock()
        snake_move_event = pygame.USEREVENT + 1
        pygame.time.set_timer(snake_move_event, game_delay)
        self.log_event("GAMEDELAYSET ", str(game_delay))
        start_time = time()
        self.snake.move()
        is_running = True
        print(Color.warning + "Waiting for a heartbeat to be detected..." + Color.normal)
        while is_running:
            clock.tick(40)
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    is_running = False
                elif self.finger_detected:
                    if event.type == pygame.KEYDOWN and event.key in valid_keys:
                        self.snake.change_direction(event.key)
                    elif event.type == snake_move_event:
                        if increase_snake_speed_by is not None:
                            pygame.time.set_timer(snake_move_event, 0)
                            game_delay = game_delay - increase_snake_speed_by
                            increase_snake_speed_by = None
                            pygame.time.set_timer(snake_move_event, game_delay)
                            self.log_event("GAMEDELAYSET ", str(game_delay))
                        self.snake.move()
                        self.log_event("APPLESNAKEPOSITIONS",  str(time()-start_time)+", "+ self.applesnakepositions())
                        if self.snake.head.pos == self.snack.pos:
                            self.log_event("POINTMADE, ", str(time()-start_time)+", "+str(self.snack.pos))
                            self.snake.add_cube()
                            self.snack = self.get_random_snack()
                            increase_snake_speed_by = int(game_delay/80)
                            self.score = len(self.snake.body)  # increase score

                        if len(list(filter(lambda body_segment: body_segment.pos == self.snake.head.pos, self.snake.body))) > 1:
                            # handle exit condition from eating yourself
                            self.log_event("SNAKEDIED, ", str(time()-start_time)+", "+str(self.score))
                            print(Color.highlight + '\nScore: \n', len(self.snake.body), ' and you died of autocannibalism!\n' + Color.normal)
                            sleep(3)  # give time for a few HR and O2 samples and give you time to ponder your mortality
                            is_running = False
            if is_running: self.redraw_window(self.window)
        self.score = len(self.snake.body)

    def pygame_quit(self):
        pygame.quit()
class Cube:
    """
    Represents a Cube
    Cubes make up the snack and the snake segments
    """

    def __init__(self, game_width, game_rows, start, dirnx = 1, dirny = 0, color = (255, 0, 0)):
        self.pos = start
        self.dirnx = dirnx
        self.dirny = dirny
        self.color = color
        self.game_width = game_width
        self.game_rows = game_rows

    def move(self, dirnx, dirny):
        """
        move
        """
        self.dirnx = dirnx
        self.dirny = dirny
        self.pos = (self.pos[0] + self.dirnx, self.pos[1] + self.dirny)

    def draw(self, surface, eyes = False):
        """
        draw
        """
        dis = self.game_width // self.game_rows
        i = self.pos[0]
        j = self.pos[1]

        pygame.draw.rect(surface, self.color, (i*dis+1, j*dis+1, dis-2, dis-2))
        if eyes:
            spacing = int(dis/5)
            radius =  int(dis/8)
            if self.dirnx == -1:
                e1x = -1
                e2x = -1
                e1y = -1
                e2y = +1
            elif self.dirnx == 1:
                e1x = 1
                e2x = 1
                e1y = -1
                e2y = 1
            elif self.dirny == -1:
                e1x = -1
                e2x = 1
                e1y = -1
                e2y = -1
            elif self.dirny == 1:
                e1x = -1
                e2x = 1
                e1y = 1
                e2y = 1
            eye1 = (i*dis+int(dis/2)+e1x*spacing, j*dis+int(dis/2)+e1y*spacing)
            eye2 = (i*dis+int(dis/2)+e2x*spacing, j*dis+int(dis/2)+e2y*spacing)
            #circle_middle2 = (i*dis + dis - radius*2, j*dis+8)
            pygame.draw.circle(surface, (70, 130, 140), eye1, radius)
            pygame.draw.circle(surface, (100, 70, 180), eye2, radius)

class Snake:
    """
    Snake represents the snake in the snake game. Snake.
    """

    def __init__(self, game_width, game_rows, color, pos):
        self.game_width = game_width
        self.game_rows = game_rows
        self.body = []
        self.turns = {}
        self.color = color
        self.head = Cube(game_width, game_rows, pos)
        self.body.append(self.head)
        self.dirnx = 0
        self.dirny = 1

    def change_direction(self, direction):
        """
        move
        """
        if direction == pygame.K_LEFT:
            self.dirnx = -1
            self.dirny = 0
            self.turns[self.head.pos[:]] = [self.dirnx, self.dirny]
        elif direction == pygame.K_RIGHT:
            self.dirnx = 1
            self.dirny = 0
            self.turns[self.head.pos[:]] = [self.dirnx, self.dirny]
        elif direction == pygame.K_UP:
            self.dirnx = 0
            self.dirny = -1
            self.turns[self.head.pos[:]] = [self.dirnx, self.dirny]
        elif direction == pygame.K_DOWN:
            self.dirnx = 0
            self.dirny = 1
            self.turns[self.head.pos[:]] = [self.dirnx, self.dirny]

    def move(self):
        """
        move
        """
        for index, cube in enumerate(self.body):
            pos = cube.pos[:]
            if pos in self.turns:
                turn = self.turns[pos]
                cube.move(turn[0], turn[1])
                if index == len(self.body)-1:
                    self.turns.pop(pos)
            else:
                if cube.dirnx == -1 and cube.pos[0] <= 0:
                    cube.pos = (cube.game_rows-1, cube.pos[1])
                elif cube.dirnx == 1 and cube.pos[0] >= cube.game_rows-1:
                    cube.pos = (0, cube.pos[1])
                elif cube.dirny == 1 and cube.pos[1] >= cube.game_rows-1:
                    cube.pos = (cube.pos[0], 0)
                elif cube.dirny == -1 and cube.pos[1] <= 0:
                    cube.pos = (cube.pos[0], cube.game_rows-1)
                else:
                    cube.move(cube.dirnx, cube.dirny)

    def reset(self, pos):
        """
        reset
        """
        self.head = Cube(self.game_width, self.game_rows, pos)
        self.body = []
        self.body.append(self.head)
        self.turns = {}
        self.dirnx = 0
        self.dirny = 1

    def add_cube(self):
        """
        add_cube
        """
        tail = self.body[-1]

        if tail.dirnx == 1 and tail.dirny == 0:
            self.body.append(
                Cube(self.game_width, self.game_rows, (tail.pos[0]-1, tail.pos[1])))
        elif tail.dirnx == -1 and tail.dirny == 0:
            self.body.append(
                Cube(self.game_width, self.game_rows, (tail.pos[0]+1, tail.pos[1])))
        elif tail.dirnx == 0 and tail.dirny == 1:
            self.body.append(
                Cube(self.game_width, self.game_rows, (tail.pos[0], tail.pos[1]-1)))
        elif tail.dirnx == 0 and tail.dirny == -1:
            self.body.append(
                Cube(self.game_width, self.game_rows, (tail.pos[0], tail.pos[1]+1)))

        self.body[-1].dirnx = tail.dirnx
        self.body[-1].dirny = tail.dirny

    def draw(self, surface):
        """
        draw
        """
        for cube in self.body:
            cube.draw(surface)
        self.body[0].draw(surface, True)
