# !/usr/bin/env python3

from queue import Queue, Empty
from threading import Thread
from time import sleep
from time import monotonic
from classes.color import Color
import sqlite3
import uuid


def millis():
    """[returns a monotonic ms, good for timing
        -- never goes backwards cases of clock adjustments]
    """
    return round(monotonic() * 1000)


class DataRecorder:
    """[The DataRecorder provides all the services to 1) write data
        to the database 2) Manage the users]

    # usage:
    #     myDataLogger = newDataLogger(settings)
    #     myDataLogger.confirm_current_user()
    #           also starts the session and returns
    #     myDataLogger.end_session()
    #           records for 3 more seconds and then stops logging
    #           (data is received but discarded)
    #           restart with confirm_current_user()
    #     myDataLogger.add("my info")

    # To log the serial port data example:
    #     def main():
    #         # initialize the recorder and the serial connection
    #               (they are seperate so that multiple data streams can be
    #                easily connected)
    #         myDataLogger = DataLogger(settings)

    #         # start the recording
    #         myDataLogger.confirm_user()

    #         # add a message to the log (a timestamp and ordered number is
    #                                         automatically added)
    #         myDataLogger.add("messsage")

    #         # end the recording
    #         myDataLogger.end_session()
    """

    def __init__(self, settings):
        """[Prepares the database writer]

        Arguments:
            settings {[ConfigParser]} -- [reads the settings from settings.ini]
        """
        self._started = False
        self._settings = settings
        self._userid = None
        self._close_now_flag = False
        self._all_records_written = False
        self._data_record_count = 0
        self._current_session_id = None
        self._current_user_id = None
        self._current_user_nickname = None
        self._current_user_games_played = None
        self.finger_detected = False  # latest finger status of the sensor
        self.add_lock = False
        self._write_all = self._settings["database"].getboolean("record_all")
        self._write_delay = int(
            self._settings["database"]["buffer_write_delay"])
        self._max_queue = int(self._settings["database"]["max_queue_size"])
        if (self._write_all):
            print("Record all set")

        # create a database connection for everything
        # except the write thread which has it's own connection
        self._database_name = self._settings["database"]["sqlite_database"]
        if self._database_name == "default":
            self._database_name = self.get_machine_id()+".db"
        self._database_string = self._settings["database"]["sqlite_directory"] + self._database_name
        self.db_connection = sqlite3.connect(self._database_string)
        print("Connected to the database")
        self.create_tables()
        self._write_queue = Queue(self._max_queue)
        self._write_thread = Thread(target=self.write, args=(), daemon=True)
        self._write_thread.start()

    def millis(self):
        """[returns a monotonic ms, good for timing -- never goes backwards cases of clock adjustments]
        """
        return round(monotonic() * 1000)

    def create_tables(self):
        """
        Creates the database tables if they don't already exist
        """
        sql = "select datetime('now')"
        self.sql_query(sql)
        sql = """CREATE TABLE IF NOT EXISTS users (
                                            user_id INTEGER PRIMARY KEY AUTOINCREMENT,
                                            machine_id text NOT NULL,
                                            nickname text NOT NULL,
                                            start_date TEXT default CURRENT_TIMESTAMP,
                                            unique (machine_id, nickname)
                                        );"""
        self.sql_query(sql)
        sql = """CREATE TABLE IF NOT EXISTS games (
                            game_id integer PRIMARY KEY AUTOINCREMENT,
                            game_datetime TEXT DEFAULT CURRENT_TIMESTAMP,
                            game_score integer,
                            game_length integer,
                            user_id integer NOT NULL,
                            FOREIGN KEY (user_id) REFERENCES users (user_id)
                        );"""
        self.sql_query(sql)
        sql = """
                                    CREATE TABLE IF NOT EXISTS log (
                                        created_date TEXT default CURRENT_TIMESTAMP,
                                        session_id integer not null,
                                        primary_identifier text,
                                        relative_sequence text,
                                        secondary_identifier text,
                                        value text not null,
                                        FOREIGN KEY (session_id) REFERENCES games (game_id)
                                    );
                                    """
        self.sql_query(sql)

    def set_finger(self, finger_detected):
        """[Holds the most recent finger status for user feedback purposes, should
            not be considered authoratative or used for data processing]
        """
        try:
            self.finger_detected = bool(finger_detected)
        except ValueError as error:
            print(Color.error + "Invalid HR, should be integer or convertible to integer: " + Color.normal, error)

    def sql_query(self, query, values=None):
        """performs a simple database query

        Arguments:
            query {[string]} -- [sqlite query]
            values {[tuple]} -- [tuple of values to insert (see sqlite docs)]
        Returns:
            db_cursor
            Note: do what you need and don't hang on to the cursor
        """
        try:
            db_cursor = self.db_connection.cursor()
            if values is None:
                db_cursor.execute(query)
            else:
                db_cursor.execute(query, values)
            self.db_connection.commit()
        except sqlite3.Error as failure:
            print("database error: ", failure)
        return db_cursor

    def get_recent_users(self):
        """
        returns all the player on this machine with the most recent first
        :return: list of users names, id and date last game played
        """
        query = """SELECT distinct NICKNAME, users.user_id, MAX(datetime(games.game_datetime))
            FROM users
            LEFT JOIN games
            ON users.user_id = games.user_id
            WHERE users.machine_id = ? ORDER BY datetime(games.game_datetime) desc
            """
        values = (str(self.get_machine_id()), )
        return self.sql_query(query, values)

    def confirm_current_user(self):
        """
        confirms a user  (prompts with last user)
        note: a user  nickname is only unique to a machine running the lab
        """
        # now figure out if new user or old user and setup appropriately
        users = self.get_recent_users().fetchall()
        default_name = ""
        users_names = []
        print("\n")
        if users[0][0] is not None:
            default_name = users[0][0]
            for user in users:
                users_names.append(user[0])
            print(Color.highlight + "Existing usernames: " + Color.normal + Color.warning + ", ".join(users_names))
            print(Color.highlight + 'Last user on this machine was: ' +
                  Color.warning + default_name + Color.highlight + ' are you back for more?')
            print(Color.error + '    [enter] for yes or type a [new name]' + Color.normal)
        else:
            print(Color.error + 'Please enter a nickname to keep track of your games (for science!)' + Color.normal)
        do_loop = True
        while do_loop:
            input_name = input(' > ')
            input_name = input_name.upper()
            if input_name == "" and default_name != "":
                input_name = default_name
            if input_name != "":
                do_loop = False
                if input_name not in users_names:
                    print(Color.highlight + "Create a new user: " + Color.warning + input_name + Color.highlight + "? (y/n)" + Color.normal)
                    input_confirm = input(" > ")
                    if len(input_confirm) > 0 and input_confirm[0:1] == "y":
                        self.create_user(input_name)
                    else:
                        print(Color.warning + 'Please enter a nickname so I can keep track of your games!' + Color.normal)
                        do_loop = True
        self.set_current_user(input_name)

    def start_session(self):
        """
        Create a new game
        and sets the started flag
        """
        sql = """INSERT INTO games(user_id)
                    VALUES(?) """
        values = (self._current_user_id, )
        last_row_id = self.sql_query(sql, values).lastrowid
        sql = """SELECT game_id
                    FROM games
                    where rowid = ?
                    """
        values = (last_row_id, )
        result = self.sql_query(sql, values).fetchone()
        self._current_session_id = result[0]
        self._started = True
        print("Data Recording Session Started: ", self._current_session_id)
        return self._current_session_id

        self.add("DataRecorder", millis(), "MSG", "START SESSION, "+str(self._current_session_id))

    def set_current_user(self, nickname):
        """
        Counts how many games a user has played, used to adjust gamespeed
        Also sets the user_id from the nickname (and current machine)
        :param conn: connection to database
        :param: nickname (already determined valid)
        """
        self._current_user_nickname = nickname

        sql = """SELECT user_id
                FROM users
                WHERE nickname = ? and machine_id = ?
                """
        values = (nickname, self.get_machine_id())
        result = self.sql_query(sql, values).fetchone()
        self._current_user_id = result[0]
        sql = """SELECT COUNT(games.user_id) FROM games
                WHERE user_id = ?
                """
        results = self.sql_query(sql, (self._current_user_id, )).fetchone()
        self._current_user_games_played = int(results[0])

    def create_user(self, nickname):
        """
        Create a new user into the users table
        :return:
        """
        sql = ''' INSERT INTO users(machine_id, nickname) VALUES (?, ?)'''
        self.sql_query(sql, (self.get_machine_id(), nickname))

    def get_machine_id(self):
        """ return the UUID portion of UUID1 that represents the machine ID
        :param none
        :return: UUID specific to machine
        """
        return str(uuid.uuid1())[-9:]

    def add_error(self, error_string):
        """[specifically adds an error entry in the log]

        Arguments:
            error_string {[string]} -- [a description of the error]
        """
        self.add("DataRecorder", millis(), "ERR", error_string)

    def add(self, primary_identifier, relative_sequence, secondary_identifier, value):
        """"
        This is the function that other classes can add a message to the log file,
        it is the gatekeeper and prevents simulaneous writes from happening.
        It adds the data to a write safe queue quickly so that this function remains
        available to other classes attempting to access it.  The write thread does the
        actual writing in the order that the data was "added"
        [All log entries are kept simple here but also structured so that they can be easily
             transformed into a dataframe for machine learning. ]"
        Arguments:
            primary_identifier {[string]} -- [Typically the device or source of the data, like the SNAKEGAME or REDBOARD]
            secondary_identifier {[string]} -- [The identifier for the  the primary identifier, like HR or GAMESCORE]
            value {[anything convertible to a string or byte string]} --
            relative_sequence {[int]} -- some kind of identifier that orders the data relative to other measurement of the same identifiers,
                or a sequence.  If it is left blank then a monotonic (guaranteed to be increasing) ms is used from when add is called.

        Keyword Arguments:
            sequence {[type]} -- [description] (default: {None})
        """
        while (self.add_lock):  # wait if add is already running
            sleep(0.001)
        self.add_lock = True  # block other adds while processing
        if (self._write_queue.full()):  # if the queue is full, drop the oldest data
            self._write_queue.get()
        # add the item if we are recording everything or the session has started
        if self._write_all or self._started:
            self._write_queue.put((primary_identifier.strip(), relative_sequence, secondary_identifier.strip(), value.strip()))
            self._data_record_count += 1
        self.add_lock = False  # release the block and exit

    def on_close(self):
        """[this is called by the system when the main program is closed]
        """
        self._close_now_flag = True

    def write(self):
        """This write thread is self contained so that data can be queued as it comes in
        and written to the database as processing time and bandwidth permits without
        slowing the system down. Because of sqlite requirements it must have it's own
        database connection as well and so handles it's own connection and writes.
        """
        write_thread_db_conn = sqlite3.connect(self._database_string)

        running = True
        while running:
            while self._current_session_id is not None:
                try:
                    (primary_identifier, relative_sequence, secondary_identifier, value) = self._write_queue.get(True, 1)
                    # db system write
                    sql = ''' INSERT INTO log(session_id, primary_identifier, relative_sequence, secondary_identifier, value)
                        VALUES(?, ?, ?, ?, ?)'''
                    cursor = write_thread_db_conn.cursor()
                    cursor.execute(sql, (self._current_session_id, primary_identifier, relative_sequence, secondary_identifier, value))
                    write_thread_db_conn.commit()
                except Empty:
                    if self._close_now_flag:
                        write_thread_db_conn.close()
                        running = False
                    sleep(self._write_delay)  # wait this many seconds if the write buffer is emptied
            else:  # session_id isn't set so just wait a bit and try again
                sleep(self._write_delay)

    def end_session(self):
        """[stops the game session and resets the session_id]
        """

        self.add("DataRecorder", millis(), "MSG", "Data Recorder stopping  with " + str(self._data_record_count) + " records written.")
        self.add("DatatRecorder", millis(), "MSG", "Record stop")
        self._started = False
        sleep(3)
        self._write_thread.isDaemon
        self.most_recent_hr = 0
        self.current_session_id = None
