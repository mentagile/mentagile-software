# !/usr/bin/env python3
class Color:
    """Ansi color codes for console output
    """
    error = "\033[1;31m"
    warning = "\033[1;33m"
    normal = "\033[1;34m"
    bold = "\033[1;35m"
    highlight = "\033[1;32m"


if __name__ == "__main__":
    """execute only if run as a script
    """
    print(Color.error + "Error")
    print(Color.warning + "warning")
    print(Color.bold + "bold")
    print(Color.highlight + "highlight")
    print(Color.normal + "normal")
